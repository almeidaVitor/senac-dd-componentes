/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.componentes.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class conexaoDB {
   
    public static void main (String[] args){
        conexaoDB conDB = new conexaoDB();
    System.out.println(conDB.getConnection());
    }
    
    private static Connection conn = null;
    public Connection getConnection(){
        try {
            if (conn != null && conn.isClosed() == false){
                return conn;
            }
        }
        catch (SQLException ex) {
            //não fazer nada, tenta conectar novamente
        }
            
        Connection conn = null;
        String porta = "3306";
        String servidor = "localhost";
        String usuario = "root";
        String senha = "";
        String bancoDados = "projetos";
        String url = "jdbc:mysql://"+servidor+":"+porta+"/"+bancoDados;
        try{
            conn = DriverManager.getConnection(url,usuario,senha);
            
        }catch (SQLException ex){
            throw new RuntimeException("Problemas na conexão com o Banco de dados"+ ex.getMessage(), ex);
        }
         return conn;
    }
} 
