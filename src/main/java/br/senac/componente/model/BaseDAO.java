/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.componente.model;

/**
 *
 * @author Aluno
 */
public interface BaseDAO <A, B> {

   public A getPorId (B id);
    
}
