/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.componente.exception;

/**
 *
 * @author Aluno
 */
public class ParametroInvalidosException extends RuntimeException {

    public ParametroInvalidosException() {
    }

    public ParametroInvalidosException(String message) {
        super(message);
    }

    public ParametroInvalidosException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParametroInvalidosException(Throwable cause) {
        super(cause);
    }

    public ParametroInvalidosException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
